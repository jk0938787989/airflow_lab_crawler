import requests
import pandas as pd
from datetime import date
from bs4 import BeautifulSoup
from datetime import datetime


def get_data():
    headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'}
    res = requests.get('https://news.ltn.com.tw/list/breakingnews',headers = headers)
    print(res)
    soup = BeautifulSoup(res.text, 'html.parser')
    return soup


def transfer_data(soup):
    df = pd.DataFrame()
    curr_date = date.today().strftime("%Y%m%d")
    for tmp in soup.select('.tit'):
        title = tmp.select('.title')[0].text
        time = tmp.select('.time')[0].text[-5:]
        url = tmp.get('href')
        key = date.today().strftime("%Y%m%d") + url[-4::]
        df = df.append(pd.DataFrame({'KEY_ID':key,'TITLE':title,'DATE':curr_date,'TIME':time,'URL':url},index=[0]))
        
    df = df.reset_index(drop=True)
    return df

if __name__ == '__main__':
    now = datetime.now()
    soup = get_data()
    df = transfer_data(soup)
    df.to_csv("/home/kp/airflow/plugins/crawler/crawler_data/crawler_" + now.strftime("%Y%m%d_%H%M%S") + ".csv", index=False, encoding= 'utf-8', header=True)
    #res = now.strftime("%Y-%m-%d %H:%M:%S") + 'Script Done!!'
    print(now.strftime("%Y-%m-%d %H:%M:%S"),'Script Done!!')
    
