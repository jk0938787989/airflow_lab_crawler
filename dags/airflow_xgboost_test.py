# -*- coding: utf-8 -*-
 
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
 
#-------------------------------------------------------------------------------
# these args will get passed on to each operator
# you can override them on a per-task basis during operator initialization
 
default_args = {
    'owner': 'KP',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['leekaiping@cathaylife.com.tw'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}
 
#-------------------------------------------------------------------------------
# dag
 
dag = DAG(
    'xgboost_dag_test',
    default_args=default_args,
    description='Xgboost_DAG',
    schedule_interval='*/1 * * * *')
 
#-------------------------------------------------------------------------------
# first operator

post = 'curl -H "Content-Type:application/json" -H "Accept:application/json" -X POST -d \"{\\"fixed acidity\\": \\"6.2\\", \\"volatile acidity\\": \\"0.56\\", \\"citric acid\\":\\"0.09\\", \\"residual sugar\\": \\"1.7\\", \\"chlorides\\": \\"0.053\\", \\"free sulfur dioxide\\": \\"24\\", \\"total sulfur dioxide\\": \\"32\\", \\"density\\": \\"0.99402\\", \\"pH\\": \\"3.54\\", \\"sulphates\\": \\"0.6\\", \\"alcohol\\": \\"11.3\\"}\" '
url = 'http://127.0.0.1:5001/xgboost_model'

api = post + url

get_result = BashOperator(
    task_id ='get_result',
    bash_command = api,
    xcom_push = True,
    dag =dag)
#-------------------------------------------------------------------------------
# second operator
 
def print_hello_xgboost():
    return 'Hello XGBoost Model!'
 
hello_operator = PythonOperator(
    task_id='hello_task',
    python_callable=print_hello_xgboost,
    dag=dag)
 
#-------------------------------------------------------------------------------
# dependencies
hello_operator >> get_result
