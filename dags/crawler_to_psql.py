import psycopg2
# psql package
import os
import shutil
import airflow
from datetime import datetime, timedelta
from airflow import DAG
# Operators; we need this to operate!
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization

default_args = {
    'owner': 'KP',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['leekaiping@cathaylife.com.tw'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}



def csv_import_psql():
    outputPath = '/home/kp/airflow/plugins/crawler/crawler_data/'
    backupPath = '/home/kp/airflow/plugins/crawler/db_backup_data/'
    allfilelist = os.listdir(outputPath)

    dojob = True

    try:
        conn = psycopg2.connect(host='localhost',  dbname='postgres', user='postgres',password='kp')
        cur = conn.cursor()
    except:
        res = 'PostgreSQL Connection Error ...'
        dojob = False
    if dojob == True:
        try:
            allfilelist = os.listdir(outputPath)
            print('code1 ok')
            for csv_data in allfilelist:
                print('code2 ok')
                with open(outputPath + csv_data, 'r') as f:
                    next(f)  # Skip the header row
                    print('code3 ok')
                    cur.copy_from(f, 'crawler', sep=',') # Copy data to psql
                    print('code4 ok')
                    shutil.move(outputPath + csv_data, backupPath + "bak_" + csv_data) # move data to backup file
                    print('code5 ok')
                conn.commit()
            res = '[Succesful] Data to Psql and backup done. '
            return res
        except:
            res = 'Read File Error ... '
            return res
    else:
        res = 'Connection Faise ...'
        return res



dag = DAG(
    dag_id='crawler_to_psql_dag',
    description='Using crawler to get news title, and import to postgreSQL.',
    default_args=default_args,
    schedule_interval='*/1 * * * *'
)

# Phase 1 : Exce Crawler form news

crawler_news_task = BashOperator(
    task_id = 'crawler_news',
    bash_command = 'python3 /home/kp/airflow/plugins/crawler/main.py',
    xcom_push = True,
    dag = dag
    )

# Phase 2 : Import CSV to PostgreSQL and put data to backup file 


import_csv_to_psql_task = PythonOperator(
    task_id='import_csv_to_psql',
    python_callable=csv_import_psql,
    dag=dag
)


crawler_news_task >> import_csv_to_psql_task 


