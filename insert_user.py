import getpass
import airflow
from airflow import models, settings
from airflow.contrib.auth.backends.password_auth import PasswordUser

def inser_user(username, email, password, is_superuser):
    user = PasswordUser(models.User())
    user.username = username    
    user.email = email      
    user.password = password    
    user.superuser = is_superuser
    session = settings.Session()
    session.add(user)       
    session.commit()
    session.close()


if __name__ == '__main__':
    username = input("Enter your username:")
    email = input("Enter your email:")
    is_superuser = input("Is super user(Y/N)?")
    is_superuser = 1 if is_superuser.upper() == 'Y' else 0
    password1 = getpass.getpass("Enter your password:")
    password2 = getpass.getpass("Please re-enter your password:")
    if password1 != password2:  
        print("Passwords entered twice are inconsistent!")
    else:   
        inser_user(username=username, email=email, password=password1, is_superuser=is_superuser)
        print("Success!")
